## Instalação

Crie um projeto contendo apenas o composer.json como conteúdo:

```
{
    "name": "name/project",
    "require": {
      "php": "^8.3",
      "nextstage-brasil/ns-library": "^1"
    },
    "license": "Apache-2.0",
    "autoload": {
      "psr-4": {
        "Your\\Path\\": "src/"
      }
    },
    "scripts": {
      "nslibrary-install": [
        "NsLibrary\\Install\\Install::handle"
      ]
    },
    "require-dev": {
      "phpunit/phpunit": "*"
    }
  }

```

e execute o comando:

```
composer install
composer run-script nslibrary-install
```
