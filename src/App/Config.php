<?php

namespace NsLibrary\App;

use NsUtil\EnvFile;
use NsUtil\Helper;


class Config
{

    private static $cfg;

    private function __construct() {}

    public static function init(array $SistemaConfig = [])
    {
        self::$cfg = new \NsUtil\Config([]);

        // Carregar configurações extras
        self::$cfg->loadFromPathConfig(Helper::getPathApp() . '/src/config/auto');

        $_CONFIG = file_exists(Helper::getPathApp() . '/.env')
            ? array_merge(EnvFile::loadEnvFile(Helper::getPathApp() . '/.env'), getenv())
            : getenv();

        // Padrão
        $_CONFIG['psr4Name'] = Helper::getPsr4Name();
        $_CONFIG['path'] = Helper::getPathApp();
        $_CONFIG['pathTmp'] = Helper::getTmpDir();
        $_CONFIG['DBPASS'] = str_replace('"', '', $_CONFIG['DBPASS'] ?? 'not-defined');
        $_CONFIG['autoload'] = $_CONFIG['path'] . '/cron/autoload.php';
        $_CONFIG['url'] = isset($_CONFIG['REQUEST_SCHEME'])
            ? $_CONFIG['REQUEST_SCHEME'] . '://' . $_CONFIG['HTTP_HOST']
            : 'console-application';

        // Configuração padrão de database
        $_CONFIG['database'] = [
            'host' => $_CONFIG['DBHOST'] ?? '',
            'user' => $_CONFIG['DBUSER'] ?? '',
            'pass' => $_CONFIG['DBPASS'] ?? '',
            'port' => $_CONFIG['DBPORT'] ?? 5432,
            'dbname' => $_CONFIG['DBNAME'] ?? '',
        ];

        // Default configs
        $configuracao = array_merge($SistemaConfig, $_CONFIG);
        self::$cfg->setByArray($configuracao, true);

        // Configurações sobreescritas
        self::$cfg->loadFromPathConfig($_CONFIG['path'] . '/src/config');
    }

    public static function getAll(): array
    {
        return self::$cfg->getAll();
    }

    public static function getData($key)
    {
        return self::$cfg->get($key);
    }

    public static function setData($key, $val)
    {
        return self::$cfg->set($key, $val);
    }
}
