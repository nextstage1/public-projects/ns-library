<?php

namespace NsLibrary\App\Singleton;

use NsUtil\Api;

class Request
{
    private static ?Api $instance = null;

    public static function getInstance(): Api
    {
        if (self::$instance === null) {
            self::$instance = new Api();
        }

        return self::$instance;
    }

    public static function setInstance(Api $api): void
    {
        self::$instance = $api;
    }
}
