<?php

namespace NsLibrary\App;

use Exception;
use NsUtil\Helper;
use NsLibrary\SistemaLibrary;
use NsUtil\ConnectionPostgreSQL;
use NsUtil\Log;

use function NsUtil\env;

class App
{

    private static $con;

    /**
     * Inicia as configurações da aplicação e itens de segurança
     */
    public static function init()
    {
        Config::init();
        SistemaLibrary::initByConfig(Config::getAll());
        SistemaLibrary::setSecurity(0);

        SistemaLibrary::setPermissionFunction(function () {
            return true;
        });

        $isDevelopment = Helper::compareString(1, (int) env('DEBUG_MODE', 0)) || strlen(env('BUILD_APP_NAME', '')) > 0;

        if ($isDevelopment) {
            self::setDevelopMode();
        } else {
            self::setProductionMode();
        }

        // Default path to php error logs
        Helper::mkdir('/tmp/php', 0777);
        ini_set('error_log', "/tmp/php/php-error.log");

        date_default_timezone_set(env('TIMEZONE', 'UTC'));
    }

    public static function setDevelopMode()
    {
        error_reporting(E_ALL);
    }

    public static function setProductionMode()
    {
        error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
    }

    /**
     * Retorna uma conexão
     * É necessário existir a configuração de Config para tal
     * @return ConnectionPostgreSQL
     */
    public static function getConnection(): ConnectionPostgreSQL
    {
        if (!self::$con) {
            try {
                self::$con = ConnectionPostgreSQL::getConnectionByEnv();
            } catch (Exception $exc) {
                echo $exc->getMessage();
                die();
            }
        }
        return self::$con;
    }

    public static function log(string $type, $content, bool $ignoreBacktrace = false): void
    {
        if (env('ENVIRONMENT', 'main') !== 'main') {
            Helper::saveFile(env('APP_LOG_PATH', Helper::getPathApp() . '/storage/logs/.htaccess'), false, 'require all denied', 'SOBREPOR');
            Log::logTxt(
                env('APP_LOG_PATH', Helper::getPathApp() . '/storage/logs') . '/nslibrary.log',
                "[$type] " . var_export($content, true),
                $ignoreBacktrace
            );
        }
    }

    /**
     * @abstract use setDevelopMode
     *
     * @return void
     */
    public static function setDevelopeMode()
    {
        self::setDevelopMode();
    }
}
