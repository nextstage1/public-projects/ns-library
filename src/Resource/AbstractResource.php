<?php

namespace NsLibrary\Resource;

use NsUtil\Helper;

/**
 * Classe abstrata que implementa a interface ResourceInterface e fornece funcionalidades básicas para manipulação de recursos.
 */
abstract class AbstractResource implements ResourceInterface
{
    /**
     * Recurso subjacente que esta classe manipula.
     * @var mixed
     */
    protected $resource;

    /**
     * Campos a serem ignorados ao converter o recurso para um array.
     * @var array
     */
    protected array $fieldsToIgnore = [];

    protected ?array $fillable = null;

    protected array $extras = [];

    protected array $parser = [];

    /**
     * Construtor da classe.
     *
     * @param mixed $resource O recurso a ser manipulado pela classe.
     */
    public function __construct($resource)
    {
        $this->resource = $resource;
    }


    /**
     * Converte o recurso para um array, excluindo os campos definidos para serem ignorados.
     *
     * @return array O array resultante após a exclusão dos campos especificados.
     */
    public function toArray(bool $internal = false): array
    {
        // Verifica se o recurso é nulo e retorna um array vazio se for o caso.
        if (is_null($this->resource)) {
            return [];
        }

        // if (is_array($this->resource) && is_object($this->resource[0] ?? null)) {
        //     $out = [];
        //     $itens = $this->resource;
        //     foreach ($itens as $resource) {
        //         $this->resource = $resource;
        //         $out[] = $this->toArray();
        //     }
        //     return $out;
        // } else {

        if (is_array($this->resource) && !$internal) {
            $out = [];
            $itens = $this->resource;
            foreach ($itens as $key => $resource) {

                if (!is_int($key)) {
                    $this->resource = $itens;
                    return $this->toArray(true);
                }


                $this->resource = $resource;
                $out[] = $this->toArray(true);
            }
            return $out;
        } else {

            // Obtém o item como um array se for um array, caso contrário, chama o método toArray().
            $item = is_array($this->resource)
                ? $this->resource
                : $this->resource->toArray();

            if ($this->fillable !== null) {
                // retorna somente os campos marcados como fillable
                // $out = array_intersect_key($item, array_flip($this->fillable));
                $out = [];
                foreach ($this->fillable as $key => $value) {
                    // Se a chave for uma string, mapear o nome do campo para o valor setado
                    if (is_string($key)) {
                        $out[$key] = $item[$value] ?? null;
                    } else {
                        $out[$value] = $item[$value] ?? null;
                    }
                }
            } else {

                // Encontra as chaves que não devem ser ignoradas.
                $keysDifference = array_diff_key($item, array_flip($this->fieldsToIgnore));

                // Retorna o array resultante após a exclusão dos campos especificados.
                $out = array_intersect_key($item, $keysDifference);
            }

            $this->_parsersExecute($out);

            foreach ($this->extras as $key => $closure) {
                if (is_callable($closure)) {
                    $out[$key] = call_user_func($closure, $item);
                } else {
                    $out[$key] = $closure;
                }
            }

            ksort($out);
            return $out;
        }
    }

    protected function _parsersExecute($out)
    {
        foreach ($this->parser as $key => $type) {
            if (!isset($out[$key])) {
                continue;
            }
            if (is_callable($type)) {
                $this->extras[$key] = $type;
            } else {
                $this->extras[$key] = fn($item) => Helper::getValByType($item[$key], $type);
            }
        }
    }
}
