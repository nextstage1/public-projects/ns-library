<?php

namespace NsLibrary\Resource;

interface ResourceInterface
{
    public function toArray(): array;
}
