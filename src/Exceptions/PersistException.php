<?php

namespace NsLibrary\Exceptions;

use Exception;

class PersistException extends Exception
{
    private static $DBERRORS = [
        '01000' => 'Warning: General warning',
        '01004' => 'Warning: String data, right-truncated',
        '01006' => 'Warning: Privilege not revoked',
        '01007' => 'Warning: Privilege not granted',
        '01S00' => 'Invalid connection string attribute',
        '07001' => 'Warning: Wrong number of parameters',
        '07002' => 'Warning: Count field incorrect',
        '07005' => 'Warning: Prepared statement not executed',
        '07006' => 'Warning: Restricted data type attribute violation',
        '07009' => 'Warning: Invalid descriptor index',
        '08001' => 'Client unable to establish connection',
        '08002' => 'Connection name in use',
        '08003' => 'Connection does not exist',
        '08004' => 'Server rejected the connection',
        '08006' => 'Connection failure',
        '08S01' => 'Communication link failure',
        '21S01' => 'Insert value list does not match column list',
        '22001' => 'String data right truncation',
        '22002' => 'Indicator variable required but not supplied',
        '22003' => 'Numeric value out of range',
        '22007' => 'Invalid datetime format',
        '22008' => 'Datetime field overflow',
        '22012' => 'Division by zero',
        '22015' => 'Interval field overflow',
        '22018' => 'Invalid character value for cast specification',
        '22025' => 'Invalid escape character',
        '23000' => 'Integrity constraint violation',
        '24000' => 'Invalid cursor state',
        '28000' => 'Invalid authorization specification',
        '34000' => 'Invalid cursor name',
        '3D000' => 'Invalid catalog name',
        '40001' => 'Serialization failure',
        '40003' => 'Statement completion unknown',
        '42000' => 'Syntax error or access violation',
        '42S01' => 'Base table or view already exists',
        '42S02' => 'Base table or view not found',
        '42S11' => 'Index already exists',
        '42S12' => 'Index not found',
        '42S21' => 'Column already exists',
        '42S22' => 'Column not found',
        'HY000' => 'General error',
        'HY001' => 'Memory allocation error',
        'HY004' => 'Invalid SQL data type',
        'HY008' => 'Operation canceled',
        'HY009' => 'Invalid use of null pointer',
        'HY010' => 'Function sequence error',
        'HY011' => 'Attribute cannot be set now',
        'HYT00' => 'Timeout expired',
        'IM001' => 'Driver does not support this function',
        'IM017' => 'Polling is disabled',
        '23502' => 'Null value not allowed - check constraint violation',
        '23503' => 'Foreign key violation',
        '23505' => 'Unique constraint violation',
        '23514' => 'Check constraint violation',
        '40002' => 'Transaction rollback',
        '25P02' => 'Transaction aborted due to conflicts with concurrent transaction',
        '42703' => 'Column not found'
    ];

    /**
     * Extract SQL error information from error message
     * @param string $errorMessage SQL error message
     * @return array Array with error code and details
     */
    public static function extractSqlError($errorMessage)
    {
        $pattern = '/SQLSTATE\[(\w+)\]:\s*(.*?)(?::\s*\d+\s*ERROR:\s*(.*?)(?:\nDETAIL:\s*(.*?))?$|$)/';

        if (preg_match($pattern, $errorMessage, $matches)) {
            return [
                'code' => $matches[1] ?? '',
                'state' => $matches[2] ?? '',
                'error' => $matches[3] ?? '',
                'detail' => $matches[4] ?? ''
            ];
        }

        return [
            'code' => '',
            'state' => $errorMessage,
            'error' => '',
            'detail' => ''
        ];
    }

    public function __construct($message = "", $code = 0, Exception $previous = null)
    {
        $errorInfo = self::extractSqlError($message);
        $errorCode = $errorInfo['code'];
        $detail = $errorInfo['detail'];

        // Remove field names from detail message
        $detail = preg_replace('/Key \((.*?)\)=/', 'Key value', $detail);
        $detail = preg_replace('/column "(.*?)"/', 'column', $detail);

        $friendlyMessage = isset(self::$DBERRORS[$errorCode])
            ? self::$DBERRORS[$errorCode] . ": " . $detail . " ($errorCode)"
            : $message;

        parent::__construct($friendlyMessage, $code, $previous);
    }
}
