<?php

namespace NsLibrary\Exceptions;

use Exception;

class PermissionDeniedException extends Exception
{
    public function __construct($message = null, $code = 401, $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
