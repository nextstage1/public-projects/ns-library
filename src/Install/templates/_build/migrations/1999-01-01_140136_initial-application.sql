-- Created by Nextstage DBCanvas Visual Database Modeler

-- tables
CREATE TABLE public."user" (
	id_user bigserial NOT NULL,
	created_at_user timestamp NOT NULL DEFAULT now(),
	username_user varchar(250) NOT NULL,
	password_user varchar(100) NOT NULL DEFAULT 'NOT_DEFINED'::varchar,
	name_user varchar(100) NOT NULL,
	id_company bigint NOT NULL
);

CREATE TABLE public.config (
	id_config bigserial NOT NULL,
	created_at_config timestamp NOT NULL DEFAULT now(),
	key_config text NOT NULL,
	value_config text NOT NULL,
	extras_config jsonb NULL
);

CREATE TABLE public.access_token (
	id_access_token bigserial NOT NULL,
	created_at_access_token timestamp NOT NULL DEFAULT now(),
	valid_until_access_token timestamp NOT NULL,
	hash_access_token varchar(100) NOT NULL,
	id_user bigint NOT NULL,
	appname_access_token varchar(100) NULL
);

CREATE TABLE public.file (
	id_file bigserial NOT NULL,
	created_at_file timestamp NOT NULL DEFAULT now(),
	path_file varchar(250) NOT NULL,
	filename_file varchar(250) NOT NULL,
	size_file integer NOT NULL,
	mime_file varchar(50) NOT NULL,
	extension_file varchar(15) NOT NULL,
	storage_type_file varchar(50) NOT NULL DEFAULT 'LOCAL'::varchar,
	id_company bigint NOT NULL
);

CREATE TABLE public.company (
	id_company bigserial NOT NULL,
	created_at_company timestamp NOT NULL DEFAULT now(),
	deleted_at_company timestamp NULL,
	name_company text NOT NULL,
	webhook_url_company varchar(250) NOT NULL,
	webhook_token_company varchar(100) NOT NULL
);

CREATE TABLE public.plan (
	id_plan bigserial NOT NULL,
	created_at_plan timestamp NOT NULL DEFAULT now(),
	updated_at_plan timestamp NULL,
	deleted_at_plan timestamp NULL,
	name_plan text NOT NULL,
	id_subscription bigint NOT NULL
);

CREATE TABLE public.subscription (
	id_subscription bigserial NOT NULL,
	created_at_subscription timestamp NOT NULL DEFAULT now(),
	updated_at_subscription timestamp NULL,
	deleted_at_subscription timestamp NULL,
	name_subscription text NOT NULL,
	id_plan bigint NOT NULL,
	id_company bigint NOT NULL
);

-- field and tables comments
COMMENT ON COLUMN public."user".username_user IS 'To log in application';
COMMENT ON COLUMN public."user".name_user IS 'Name of user';
COMMENT ON COLUMN public.file.id_file IS 'File';
COMMENT ON COLUMN public.file.created_at_file IS 'Created At';
COMMENT ON COLUMN public.file.path_file IS 'Absolute path on storage';
COMMENT ON COLUMN public.file.filename_file IS 'Filename to exibition';
COMMENT ON COLUMN public.file.size_file IS 'Size of file';
COMMENT ON COLUMN public.file.mime_file IS 'Mime type of file';
COMMENT ON COLUMN public.file.extension_file IS 'Extension of file';
COMMENT ON COLUMN public.file.storage_type_file IS 'Storage name';
COMMENT ON COLUMN public.company.webhook_url_company IS 'URL to send notification';
COMMENT ON COLUMN public.company.webhook_token_company IS 'Header Bearer Token to send';

-- constraints
ALTER TABLE public."user" 
	ADD CONSTRAINT user_pk PRIMARY KEY (id_user);
ALTER TABLE public."user" 
	ADD CONSTRAINT user_username_user_un UNIQUE (username_user);
ALTER TABLE public.config 
	ADD CONSTRAINT config_pk PRIMARY KEY (id_config);
ALTER TABLE public.access_token 
	ADD CONSTRAINT access_token_pk PRIMARY KEY (id_access_token);
ALTER TABLE public.file 
	ADD CONSTRAINT file_pk PRIMARY KEY (id_file);
ALTER TABLE public.company 
	ADD CONSTRAINT company_pk PRIMARY KEY (id_company);
ALTER TABLE public.plan 
	ADD CONSTRAINT plan_pk PRIMARY KEY (id_plan);
ALTER TABLE public.subscription 
	ADD CONSTRAINT subscription_pk PRIMARY KEY (id_subscription);

-- foreign keys
ALTER TABLE public.subscription
            ADD CONSTRAINT plan_subscription_fk
            FOREIGN KEY(id_plan)
            REFERENCES public.plan (id_plan)
            ON DELETE CASCADE
            ON UPDATE CASCADE; 
ALTER TABLE public.user
            ADD CONSTRAINT company_user_fk
            FOREIGN KEY(id_company)
            REFERENCES public.company (id_company)
            ON DELETE CASCADE
            ON UPDATE CASCADE; 
ALTER TABLE public.file
            ADD CONSTRAINT company_file_fk
            FOREIGN KEY(id_company)
            REFERENCES public.company (id_company)
            ON DELETE CASCADE
            ON UPDATE CASCADE; 
ALTER TABLE public.access_token
            ADD CONSTRAINT user_access_token_fk
            FOREIGN KEY(id_user)
            REFERENCES public.user (id_user)
            ON DELETE CASCADE
            ON UPDATE CASCADE; 

-- indexes
CREATE INDEX config_key_config_idx ON public.config USING btree (key_config);
CREATE INDEX config_extras_config_idx ON public.config USING gin (extras_config);
CREATE UNIQUE INDEX access_token_name_access_token_idx ON public.access_token USING btree (hash_access_token);