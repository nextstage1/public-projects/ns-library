<?php

namespace NsUtil\Middlewares;

use NsUtil\Api;
use function NsUtil\now;
use NsLibrary\App\Singleton\Request;
use NsUtil\Middlewares\CheckAPIKeyMiddleware as CheckAPIKeyMiddlewareNsutil;
use {namespace}\Auto\Models\AccessToken;

class CheckApikeyMiddleware extends CheckAPIKeyMiddlewareNsutil
{

    public function __construct()
    {
        $this->apikey = 'bearer';
        $this->freeRoutes = array_merge($this->freeRoutes, [
            'healthcheck',
            'login'
        ]);
        parent::__construct();
    }

    public function check(): bool
    {
        $user = (new AccessToken())
            ->firstOrFail([
                'hashAccessToken' => $this->apikey,
                'validUntilAccessToken' => ['>', "'" . now()->format('Y-m-d H:i:s') . "'"]
            ], 'Invalid token', Api::HTTP_FORBIDDEN);

        Request::getInstance()->request('user', $user->getUser());

        return true;
    }
}
