<?php

namespace {namespace};

use NsUtil\Helper;
use function NsUtil\env;
use function NsUtil\now;
use NsUtil\Security\Password;
use NsUtil\Services\RateLimiter;
use NsLibrary\App\Singleton\Request;
use NsLibrary\Controller\ApiRest\AbstractApiRestController;
    use NsUtil\Services\Redis;
    use {namespace}\Auto\Models\User;
use {namespace}\Auto\Models\AccessToken;
use {namespace}\Resources\AccessTokenResource;

class LoginRouter extends AbstractApiRestController
{
    public function create(): void
    {
        RateLimiter::byIp(5, Redis::MINUTE * 10, 'login');

        $credentials = Request::getInstance()->getUsernameAndPasswordFromAuthorizationHeaders();

        Request::getInstance()
            ->setBodyTreated($credentials)
            ->validateOrFail(['username|string', 'password|string']);


        $user = (new User())->firstOrFail(['usernameUser' => $credentials['username']], 'User not found', 404);

        if (!Password::verify($credentials['password'], $user->getPasswordUser())) {
            $this->response(['message' => 'Invalid password'], 401);
        }

        // max 120 minutes to expire
        $maxSessionTime = env('MAX_SESSION_TIME', 120);
        $minutesToExpire = min($maxSessionTime, $this->dados['minutes'] ?? 15);
        $expireAt = now()->add("{$minutesToExpire} minutes");

        $at = (new AccessToken([
            'idUser' => $user->getIdUser(),
            'validUntilAccessToken' => $expireAt->format('Y-m-d H:i:s'),
            'hashAccessToken' => hash('sha256', Helper::generateUuidV4())
        ]))->saveOrFail();

        RateLimiter::clear(null, 'login');

        $this->response(new AccessTokenResource($at), 200);
    }
}
