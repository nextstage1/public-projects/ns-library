<?php

namespace NsUtil\Resources;

use NsLibrary\Resource\AbstractResource;

use function NsUtil\env;

class AccessTokenResource extends AbstractResource
{

    public function __construct($resource)
    {
        parent::__construct($resource);

        $this->fillable = [
            'accessToken' => 'hashAccessToken',
            'validAt' => 'validUntilAccessToken'
        ];
    }
}
