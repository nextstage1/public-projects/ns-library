<?php

// Configure your app configs here. To get, use Config::get('keyname');

use NsUtil\EnvFile;
use function NsUtil\env;

$out =  [
    // Storage config
    'STORAGE_DRIVE' => env('STORAGE_DRIVE', 'local'),
    'BUCKET_NAME' => env('BUCKET_NAME', '/var/www/html/storage'),
    // Redis config
    'REDIS_HOST' => env('REDIS_HOST', 'host.docker.internal'),
    'REDIS_PORT' => env('REDIS_PORT', 6379),
    'REDIS_PASSWORD' => env('REDIS_PASSWORD', null),
];

EnvFile::applyEnvVariables(null, $out);

return $out;
