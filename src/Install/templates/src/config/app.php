<?php

// Configure your app configs here. To get, use Config::get('keyname');

use NsUtil\EnvFile;
use function NsUtil\env;

$out =  [
    // App config
    'APP_TITLE' => env('APP_TITLE', 'Default Application Title'),
    'MAX_SESSION_TIME' => env('MAX_SESSION_TIME', 120), // minutes
    'RATE_LIMIT_MAX_CALLS' => env('RATE_LIMIT_MAX_CALLS', 600),
    'TOKEN_CRYPTO' => env('TOKEN_CRYPTO', '{TOKEN_CRYPTO}'),
];

EnvFile::applyEnvVariables(null, $out);

return $out;
