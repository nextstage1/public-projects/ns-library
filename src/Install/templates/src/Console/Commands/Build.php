<?php

namespace NsUtil\Console\Commands;

use NsLibrary\Builder\Create;
use NsUtil\Commands\Abstracts\Command;
use NsUtil\ConsoleTable;
use NsUtil\EnvFile;
use NsUtil\Exceptions\FileAlreadyExistsException;
use NsUtil\Helper;

use function NsUtil\dd;
use function NsUtil\env;
use function NsUtil\nsCommand;

class Build extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = "app:build";

    /**
     * Handles the execution of the command.
     *
     * @param array $args The arguments passed to the command.
     * @return void
     */
    public function handle(array $args): void
    {
        $me = basename(str_replace('\\', "/", self::class));

        EnvFile::applyEnvVariables();

        $this->chmodLocal();

        echo nsCommand('migrate' . ($args[0] ?? '' === 'fresh' ? ':fresh force' : ''));

        // Builder
        $builder = new Create(
            array_map('trim', explode(',', env('BUILD_SCHEMAS', "public"))),
            [],
            array_map('trim', explode(',', env('BUILD_IGNORE_TABLES', '__notdefined__')))
        );

        ConsoleTable::printTabular(
            'Building',
            '',
            fn() =>  $builder->run(
                env('BUILD_TOKEN', ''),
                env('BUILD_APP_NAME', ''),
                env('BUILD_TAG_TITLE', ''),
                env('BUILD_ADMIN_NAME', ''),
                env('BUILD_ADMIN_EMAIL', ''),
                array_map('trim', explode(',', env('BUILD_IGNORE_ROUTERS', '__notdefined__')))
            )
        );

        $this->defaultConfig($builder);

        $this->resourcesCreate($builder);

        $this->chmodLocal();

        nsCommand('make:router healthcheck');

        $this->clear();

        echo "\n";

        $this->success($me);
    }

    private function chmodLocal(): void
    {
        shell_exec('cd ' . Helper::getPathApp() . ' && chmod 0777 . -R >/dev/null 2>&1');
    }

    private function clear(): void
    {

        $cmd = 'find %1$s -name "*__NEW__*" -delete >/dev/null 2>&1 && find %1$s -name "*XPTO*" -delete >/dev/null 2>&1 && find %1$s/app/_45h -name "*.php" -delete >/dev/null 2>&1 && find %1$s/app/45h -name "*.php" -delete >/dev/null 2>&1';
        $cmd = sprintf($cmd, Helper::getPathApp());
        ConsoleTable::printTabularAndRunningCMD('Clear files', $cmd);
    }

    private function resourcesCreate(Create $builder): void
    {
        ConsoleTable::printTabular(
            'Resources create',
            '',
            fn() =>  array_map(
                function ($item) {
                    try {
                        nsCommand('make:resource ' . $item['entidade']);
                    } catch (FileAlreadyExistsException $exc) {
                        // 'ok'
                    } catch (\Exception $exc) {
                        echo $exc->getMessage();
                    }
                },
                $builder->readData()['itens']
            )
        );
    }

    private function defaultConfig(Create $builder): void
    {
        $data = $builder->readData();
        $stringPHP = str_replace(['array (', ')'], ['[', ']'], var_export($data['config'], true));

        Helper::saveFile(
            Helper::getPathApp() . '/src/config/auto/default.php',
            false,
            "<?php 
                \n // Important: dont edit this file! This auto generated. To update configs, make a file on config dir
                \n return $stringPHP;",
            'SOBREPOR'
        );

        Helper::saveFile(
            Helper::getPathApp() . '/src/config/app.php',
            false,
            "<?php 
            \n // Configure your app configs here. To get, use Config::get('keyname');
            \n return [];"
        );

        $this->chmodLocal();
    }
}
