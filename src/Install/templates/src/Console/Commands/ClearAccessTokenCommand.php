<?php

namespace NsUtil\Console\Commands;

use NsLibrary\App\App;
use NsUtil\Commands\Abstracts\Command;

use function NsUtil\now;

class ClearAccessTokenCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = "app:clear-access-token";

    /**
     * Handles the execution of the command.
     *
     * @param array $args The arguments passed to the command.
     * @return void
     */
    public function handle(array $args): void
    {
        $me = basename(str_replace('\\', "/", self::class));

        App::getConnection()
            ->execQueryAndReturnPrepared(
                "DELETE FROM access_token WHERE valid_until_access_token < ?",
                [now('UTC')->format('Y-m-d H:i:s')]
            );

        $this->success($me);
    }
}
