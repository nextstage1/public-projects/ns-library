#!/usr/bin/with-contenv bash

set -e

echo "Appplication Config"

echo ">>> Paths default"
if [ ! -d "/tmp/php/logs" ]; then
    mkdir -p -m 777 /tmp/php/logs
fi
chmod -R 777 /tmp

if [ ! -d "/var/www/html/storage" ]; then
    mkdir -p /var/www/html/storage
fi
chmod -R 777 /var/www/html/storage

# Run the routines to start the application

# echo ">>> Migrations"
# cd /var/www/html && php nsutil migrate

# sleep 1 

# echo ">>> Queues"
# cd /var/www/html && php nsutil queue:stop force
# cd /var/www/html && php nsutil queue:worker