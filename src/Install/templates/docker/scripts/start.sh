#!/bin/bash

## Verificações iniciais
__DIR__="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
cd "$__DIR__/../../"

source $__DIR__/_init.sh

AWS_IGNORE_LOGIN=true

http up ${COMPOSE_PROJECT_NAME}_db

waitRestore ${COMPOSE_PROJECT_NAME}_db

# Builder
bash $__DIR__/build.sh
