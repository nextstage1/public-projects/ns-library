#!/bin/bash

export PG_CONF_PATH=${BUILD_PG_CONF_PATH:-"./docker/postgres/conf.d"}

# Define a variável __DIR__ com o diretório atual do script
__DIR__="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
cd "$__DIR__/../../"

# Importa a função print_header do arquivo functions.sh
if ! [ -f /tmp/functions.sh ]; then
    curl --silent https://nextstage-public.s3.amazonaws.com/scripts/_functions.sh >/tmp/functions.sh
fi
source /tmp/functions.sh

checkBranchMain
checkEnvExists
loadEnv

export PG_CONF_PATH=${BUILD_PG_CONF_PATH:-"./docker/postgres/conf.d"}