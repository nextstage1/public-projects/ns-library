#!/bin/bash

# Define a variável __DIR__ com o diretório atual do script
__DIR__="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
cd "$__DIR__/../../"

source $__DIR__/_init.sh

git config core.filemode false
git config --global core.filemode false

nsComposer "${COMPOSE_PROJECT_NAME}_app" install

docker exec "${COMPOSE_PROJECT_NAME}_app" sh -c "cd /var/www/html && php nsutil app:build"