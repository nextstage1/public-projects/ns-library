#!/bin/bash

# Define a variável __DIR__ com o diretório atual do script
__DIR__="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
cd "$__DIR__/../../"

source $__DIR__/_init.sh


printHeader "${COMPOSE_PROJECT_NAME} - REINSTALL"
echo "Atention!! You will lose all saved data!"
echo ""
read -p "Confirm remove the database?: (yes/no) " dd
DECIDE=${dd:-NO}
if [ ! $DECIDE = 'yes' ]; then
    echo "Aborted!"
    exit 3
fi
echo ""

dockerDown

# stop application
# docker-compose down --volumes --remove-orphans

# Clear data
rm -R ${PERSISTPATH} >/dev/null 2>&1

for rm in auto vendor composer.lock node_modules package-lock.json; do 
    FILE="$__DIR__/../../${rm}"
    if [ -f $FILE ]; then 
        rm $FILE
    fi
    if [ -d $FILE ]; then 
        rm -R $FILE
    fi
done 

bash "$__DIR__/start.sh"