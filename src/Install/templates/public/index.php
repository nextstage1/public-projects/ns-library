<?php

use NsUtil\Api;
use NsUtil\Helper;
use NsUtil\Middlewares\RateLimitMiddleware;
use NsLibrary\App\Singleton\Request;

use function NsUtil\env;

/**
 * This file will handle all requests to the frontend of the application.
 * The classes must implement the "list" function to return the content.
 * If necessary, other middlewares can be inserted following the rate limit model
 */

include __DIR__ . '/../cron/autoload.php';

$namespace = Helper::getPsr4Name()
    . '\\'
    . env('NSUTIL_ROUTERVIEW_NAMESPACE');


// Rate, Middlewares and Resolver
try {
    Request::getInstance()
        ->setTypeOut('html')
        ->onError(fn(array $response, int $responseCode) => true)
        ->onSuccess(fn(array $response, int $responseCode) => true)
        ->middlewares([
            RateLimitMiddleware::class
        ])
        ->rest($namespace);
} catch (Exception $exc) {
    Request::getInstance()->error($exc->getMessage(), Api::HTTP_INTERNAL_SERVER_ERROR);
}
