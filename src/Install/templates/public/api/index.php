<?php

use NsUtil\Api;
use NsUtil\Helper;
use function NsUtil\env;
use NsUtil\Middlewares\RateLimitMiddleware;
use NsLibrary\App\Singleton\Request;
use {namespace}\Middlewares\CheckApikeyMiddleware;

/**
 * This file will handle all requests to the api of the application
 * The classes must implement the functions mapped to api REST
 * If necessary, other middlewares can be inserted following the rate limit model
 */

include __DIR__ . '/../../cron/autoload.php';

$namespace = Helper::getPsr4Name()
    . '\\'
    . env('NSUTIL_ROUTERAPI_NAMESPACE');


// Rate, Middlewares and Resolver
try {
    Request::getInstance()
        ->onError(fn(array $response, int $responseCode) => true)
        ->onSuccess(fn(array $response, int $responseCode) => true)
        ->middlewares([
            RateLimitMiddleware::class,
            CheckApikeyMiddleware::class
        ])
        ->rest($namespace);
} catch (Exception $exc) {
    Request::getInstance()->error($exc->getMessage(), Api::HTTP_INTERNAL_SERVER_ERROR);
}