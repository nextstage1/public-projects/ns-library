<?php

use NsUtil\Api;
use NsUtil\EnvFile;
use NsLibrary\App\App;
use NsLibrary\App\Config;
use NsLibrary\SistemaLibrary;
use NsLibrary\App\Singleton\Request;
use NsUtil\Commands\NsUtils\Migrate;
use NsLibrary\Exceptions\PermissionDeniedException;


require __DIR__ . '/../vendor/autoload.php';

// App start. Init app instance
App::init();

// Dont show property content on error
Api::$ignoreContentOnError = true;
$api = new Api();
Request::setInstance($api);

// file .env e envs desta aplicação
EnvFile::applyEnvVariables(__DIR__ . '/../.env', [
    'NSUTIL_MODELS_NAMESPACE' => 'Auto/Models',
    'NSUTIL_ROUTERAPI_NAMESPACE' => 'Routers',
    'NSUTIL_ROUTERVIEW_NAMESPACE' => 'Views',
    'NSUTIL_RESOURCES_NAMESPACE' => 'Resources',
]);

// Definições de migrations 
Migrate::$conPreQuerys = [
    "SET audit.username = 'migrations'",
    "SET audit.userid = '-1'"
];

// metodo para validacao de permissoes 
SistemaLibrary::setPermissionFunction(function (array $params) {
    [$group, $subgroup, $action, $message] = $params;
    $message = '[DEFAULT MESSAGE NSLIBRARY] IMPLEMENTS YOUR RULES. FILE: ' . __FILE__;

    // instance of User. Setted on middleware checkAPI 
    $user = Request::getInstance()->request('user');

    // your rules to check permissions

    // response if permission denied
    throw new PermissionDeniedException($message);
});


return $api;
