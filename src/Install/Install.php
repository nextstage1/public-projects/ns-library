<?php

namespace NsLibrary\Install;

use NsUtil\ConsoleTable;
use NsUtil\DirectoryManipulation;
use NsUtil\EnvFile;
use NsUtil\Helper;

use NsUtil\Template;

use function NsUtil\dd;
use function NsUtil\env;
use function NsUtil\nsCommand;

class Install
{

    public static function handle()
    {

        if (file_exists(Helper::getPathApp() . '/nsutil')) {
            ConsoleTable::printTabular("NSLibrary Installer", 'File "nsutil" was found. Aborted.');
        } else {
            ConsoleTable::printTabular("NSLibrary Installer", '', function () {

                $path = Helper::getPathApp();

                $sobrepor = false;
                DirectoryManipulation::recurseCopy(__DIR__ . '/templates', $path, $sobrepor);

                $composer = json_decode(file_get_contents("$path/composer.json"));

                [$empresa, $name] = explode('/', $composer->name);
                $name = Helper::sanitize($name);

                (new EnvFile($path . '/.env'))->update([
                    'APP_LOG_PATH' => "$path/storage/logs",
                    'COMPOSE_PROJECT_NAME' => $name,
                    'LOCALNAME' => "{$name}_development",
                    'ENVIRONMENT' => 'develop',
                    'PHP_VERSION' => '8.3',
                    'PERSISTPATH' => "./docker/persist",
                    "DBHOST" => "{$name}_db",
                    "DBUSER" => "postgres",
                    "DBPASS" => '102030',
                    "DBNAME" => "{$name}_db",
                    "DBPORT" => 5432,
                    "DBTYPE" => 'postgres',
                    "DBSCHEMA" => 'public',
                    "PGVERSION" => 17,
                    "BUILD_APP_EXPOSED_PORT" => rand(11001, 12000),
                    "REDIS_HOST" => "host.docker.internal",
                    "REDIS_PASSWORD" => md5($name),
                    "REDIS_PORT" => rand(10000, 11000),
                    "BUILD_PG_CONF_PATH" => "./docker/postgres/conf.d",
                    "BUILD_PG_CONF_BACKUP_PATH" => "./docker/postgres/backup",
                    "BUILD_PG_EXPOSED_PORT" => rand(9000, 12000),
                    "BUILD_PG_MULTIPLE_DATABASES" => "{$name}_db",
                    'BUILD_SCHEMAS' => 'public',
                    'BUILD_IGNORE_TABLES' => 'example1,example2',
                    'BUILD_TOKEN' => hash('sha256', $name),
                    'BUILD_APP_NAME' => $name,
                    'BUILD_TAG_TITLE' => $name,
                    'BUILD_ADMIN_NAME' => '',
                    'BUILD_ADMIN_EMAIL' => '',
                    'BUILD_IGNORE_ROUTERS' => "AccessToken,Log,Config,Server,Request,User",
                    'XDEBUG_CLIENT_PORT' => rand(9001, 91009),
                    'XDEBUG_MODE' => 'debug',
                    'XDEBUG_START_WITH_REQUEST' => 'yes',
                ]);

                shell_exec("chmod 0777 $path -R");

                // ignore database connection error
                putenv('NSUTIL_RUNNER=true');

                // Files
                self::createDockerIgnoreFile();
                self::createGitIgnoreFile();

                // update namespaces
                $updatedNamespace = [
                    'src/Console/Commands/Build.php',
                    'src/Console/Commands/ClearAccessTokenCommand.php',
                    'src/Middlewares/CheckApikeyMiddleware.php',
                    'src/Routers/LoginRouter.php',
                    'src/Resources/AccessTokenResource.php',
                ];

                foreach ($updatedNamespace as $file) {
                    Helper::applyNamespace(Helper::getPathApp() . "/$file");
                }

                // namespace on files
                foreach (
                    [
                        'public/api/index.php',
                        'src/config/app.php',
                        'src/Middlewares/CheckApikeyMiddleware.php',
                        'src/Resources/AccessTokenResource.php',
                        'src/Routers/LoginRouter.php',
                    ] as $file
                ) {
                    $file = Helper::getPathApp() . "/$file";
                    Helper::saveFile($file, false, (new Template($file, [
                        'namespace' => Helper::getPsr4Name(),
                        'NSUTIL_MODELS_NAMESPACE' => 'Auto/Models',
                        'TOKEN_CRYPTO' => hash('sha256', time()),
                    ]))->render(), 'SOBREPOR');
                }
            });
            $instructions = <<<EOF
            ### NSLibrary - Instruções de uso  ### 
            - Na pasta dockers/scripts você encontrará os arquivos: 
               - para iniciar a aplicação: sudo bash docker/scripts/start.sh - Irá subir os containers.
               - para resetar tudo (inclusive database): sudo bash docker/scripts/reinstall.sh - Irá deletar o PERSIST_PATH e refazer a instalação
            - Após iniciar a start.sh pela primeira vez:
               - na pasta docker/postgres terão duas pasta:
                  - conf.d: para configurações do postgresql
                  - backup: espera um arquivo .backup (obtido pelo pgdump) com o nome exato da database, ex.: db_default.backup. A subir o banco pela primeira vez ou no reinstal, esse arquivo irá restaurar a base de dados
                  - backup: pode ter um segundo arquivo com extensao {dbname}.seed.sql contendo expressões html para popular a base inicial
            - Para obter informações, use (no container _app): php nsutil list - ali terá os comando facilitadores
            - Para construir a aplicação padrão com base no banco de dados: php nsutil app:build
            Enjoy!
    
EOF;
            echo $instructions;
        }

        echo "\n";
    }

    private static function createDockerIgnoreFile(): void
    {
        $content = <<<EOF
        # Directories
        .git/
        .gitlab/
        .vscode/
        node_modules/
        tests/
        vendor/
        storage/
        coverage/
        **/persist/
        **/.persist/

        # Files
        .env
        .env.*
        .gitignore
        .editorconfig
        .phpunit.result.cache
        phpunit.xml
        README.md
        docker-compose*.yml
        Dockerfile*

        # Logs
        *.log
        npm-debug.log*
        yarn-debug.log*
        yarn-error.log*
        **/*.log

        # Cache
        .cache/
        .npm/
        .yarn/

        # IDE specific files
        .idea/
        *.swp
        *.swo
        .DS_Store
        Thumbs.db
        EOF;

        file_put_contents(Helper::getPathApp() . '/.dockerignore', $content);
    }

    private static function createGitIgnoreFile(): void
    {
        $content = <<<EOF
        # Dependencies
        /node_modules
        /vendor
        /.pnp
        .pnp.js

        # Testing
        /coverage

        # Production
        /build
        /dist

        # Misc
        **/.DS_Store
        .env
        .env.local
        .env.development.local
        .env.test.local
        .env.production.local
        **/persist/
        **/storage/

        # Logs
        npm-debug.log*
        yarn-debug.log*
        yarn-error.log*
        **/*.log

        # IDE
        .idea/
        *.swp
        *.swo
        .config/

        # System Files
        Thumbs.db
        **/DS_Store
        EOF;

        file_put_contents(Helper::getPathApp() . '/.gitignore', $content);
    }

    private static function createGitlabCIFile(): void
    {
        $content = '
stages:
    - build
    - deploy

default:
    image: alpine:latest
    services:
        - docker:dind

docker_build:
    image: nextstage/docker:dind-aws
    interruptible: true
    only:
        - main
        - release
        - homolog
        - develop
    when: on_success
    stage: build
    before_script:
        - echo -e "[$AWS_PROFILE]\naws_access_key_id=$AWS_KEY\naws_secret_access_key=$AWS_SECRET" > ~/.aws/credentials
    script:
        - aws ecr get-login-password --region us-east-2 --profile ${AWS_PROFILE} | docker login --username AWS --password-stdin ${AWS_ECR}
        - docker build --build-arg PHP_VERSION=8.1 -t application .
        - docker tag application ${AWS_ECR}:${CI_COMMIT_BRANCH}
        - docker push ${AWS_ECR}:${CI_COMMIT_BRANCH}

docker_deploy:
    image: nextstage/docker:dind
    interruptible: true
    only:
        - main
        - release
        - homolog
        - develop
    when: on_success
    stage: deploy
    before_script:
        - echo "$SSH_PRIVATE_KEY" > ~/.ssh/id_rsa
        - chmod 600 ~/.ssh/id_rsa
    script:
        - $START_CMD

';

        file_put_contents(Helper::getPathApp() . '/.gitlab-ci.yml', $content);
    }
}
