<?php

use NsUtil\Config;

require __DIR__ . '/vendor/autoload.php';

$config = (new Config())->loadEnvFile(__DIR__ . '/.env');

$LOCAL_PROJECTS = explode(';', $config->get('LOCAL_COMPOSER', ''));

if (count($LOCAL_PROJECTS) > 0) {
    $src = realpath(__DIR__ . '/src');
    (new \NsUtil\LocalComposer())($src, $LOCAL_PROJECTS, 'nextstage-brasil/ns-library');
}
